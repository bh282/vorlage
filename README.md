Dies ist eine Vorlage für eine studentische Arbeit (Bachelor- oder Masterarbeit)
an der Universität Rostock. 

Jeder der möchte kann die Vorlage verwenden und modifizieren, es ist auch keine
Namensnennung erforderlich. Eine Verwendung dieser Vorlage ist am Lehrstuhl
IuK nicht vorgeschrieben, es können also nach Belieben auch andere Formate 
gewählt werden.

Zur vereinfachten Nutzung ist das Projekt auch in
Overleaf veröffentlicht: https://www.overleaf.com/read/bnsnjjbscjpp

[PDF-Vorschau](https://git.informatik.uni-rostock.de/iuk/vorlage/-/jobs/10866/artifacts/file/main.pdf)
[![Vorschaubild](https://git.informatik.uni-rostock.de/iuk/vorlage/wikis/uploads/6b711038d0502e4e8089bd18c90c80e4/preview_vorlagen_g.png)](https://git.informatik.uni-rostock.de/iuk/vorlage/-/jobs/10866/artifacts/file/main.pdf)